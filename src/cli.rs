use clap::Parser;

/// A software that compute the number of polycubes for a number of cubes
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    /// The number of cubes
    #[arg(short, value_parser = clap::value_parser!(u32).range(2..))]
    pub n: u32,

    #[arg(short, long, default_value_t = 7)]
    pub max_depth_parallel: u8,
}
