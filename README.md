# polycubes

A software to enumerate all the polycubes for a given number of cubes.

Port of code from https://github.com/hidny/MikeTsCubeCode to rust.

See https://github.com/mikepound/cubes and https://www.youtube.com/watch?v=g9n0a0644B4 for more information.

## Running the software

You need to have rust installed, see https://doc.rust-lang.org/book/ch01-01-installation.html.

Then for example if you want to calculate up to n=14:
```
cargo run --release -- -n 14
```

To build an even more optimized version, you can try the `./build-with-pgo-native.sh` script. The resulting binary is `target/x86_64-unknown-linux-gnu/release/polycubes2`.

## Pre-built binaries

See https://gitlab.com/dzamlo/polycubes/-/artifacts for pre-built binaries.

## License

Licensed under:

 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

